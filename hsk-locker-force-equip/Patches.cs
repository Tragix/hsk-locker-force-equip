﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace hsk_locker_force_equip {
    class Patches {
       
        public static class ClothLocker_ClothChange_Patch {
            public static void Postfix(object __instance) {
                Traverse traverser = Traverse.Create(__instance);
                Pawn pawn = traverser.Field("OwnerPawn").GetValue<Pawn>();
                foreach(Apparel apparel in pawn.apparel.WornApparel) {
                    if (!pawn.outfits.CurrentOutfit.filter.Allows(apparel)) {
                        pawn.outfits.forcedHandler.SetForced(apparel, true);
                    }
                }
                

            }
        }
    }
}
